run-all:
	cd ../apiplatform && docker-compose up
	cd ../prisma && docker-compose up
	cd ../vuejs && docker-compose up

run-all-bg:
	cd ../apiplatform && docker-compose up -d
	cd ../prisma && docker-compose up -d
	cd ../vuejs && docker-compose up -d

stop-all:
	cd ../apiplatform && docker-compose stop
	cd ../prisma && docker-compose stop
	cd ../vuejs && docker-compose stop